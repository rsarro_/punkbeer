//
//  ListBeerSpec.swift
//  PunkBeer
//
//  Created by Renato Matos on 29/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import Quick
import Nimble

@testable import PunkBeer

class ListBeerViewControllerSpec : QuickSpec {
    
    override func spec() {
        
        describe("ListBeerViewController"){
            
            it("Should initialize"){
                
                let vc = ListBeerViewController()
                
                waitUntil(timeout: 15) { done in
                    vc.getBeers {
                        done()
                    }
                }
                
                expect(vc.conforms(to: UITableViewDataSource.self)).to(beTruthy())
                expect(vc.conforms(to: UITableViewDelegate.self)).to(beTruthy())
                expect(vc.listItens.count).to(beGreaterThan(0))
                
            }
            
            it("TableView should not be nil", closure: {
                
                let vc = ListBeerViewController()
                
                waitUntil(timeout: 15) { done in
                    vc.getBeers {
                        done()
                    }
                }
                
                expect(vc.tableView).notTo(beNil())
                
            })
            
            it("Select a beer and calls the detail screen", closure: {
                
                let vc = ListBeerViewController()
                let navigation = MockNavigationController(rootViewController: vc)
                
                UIApplication.shared.keyWindow?.rootViewController = navigation
                
                waitUntil(timeout: 15) { done in
                    vc.getBeers {
                        done()
                    }
                }
                
                vc.tableView(vc.tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
                expect(navigation.pushedViewController?.isKind(of: DetailBeerViewController.self)).to(beTruthy())
                
            })
        }
    }
}
