//
//  MockNavigationController.swift
//  PunkBeer
//
//  Created by Renato Matos on 29/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import UIKit

class MockNavigationController: UINavigationController {
    
    var pushedViewController: UIViewController?
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        self.pushedViewController = viewController
        super.pushViewController(viewController, animated: true)
    }
    
}
