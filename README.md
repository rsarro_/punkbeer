# Punk Beer #

O projeto, estruturado em View Code, tem como objetivo listar cervejas a partir do consumo do serviço [Punk API](https://punkapi.com/documentation/v2).

### Estrutura de Implementação e Funcionalidades ###

* Lista de cervejas
* Paginação por Scroll
* Detalhe da cerveja selecionada
* Gráfico mostrando teor alcoólico e Escala de amargor

### Dependências ###

* SnapKit
* Alamofire
* Object Mapper
* SDWebImage
* PNChart
* NVActivityIndicatorView
* Toast-Swift
* Quick
* Nimble

### Build ###

* Rodar `bundle exec pod install` antes do build para atualizar as dependências

### Test ###

* Unitário com Quick e Nimble
* Para gerar relatório `bundle exec slather`