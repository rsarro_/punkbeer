//
//  AppDelegate.swift
//  PunkBeer
//
//  Created by Renato Matos on 28/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let controller = ListBeerViewController()
        let navigation = UINavigationController(rootViewController: controller)
        
        self.window?.rootViewController = navigation
        self.window?.makeKeyAndVisible()
        
        if let baseUrl = EnviromentUtil.baseUrl {
            APIClient.shared.setupClient(baseURL: baseUrl)
        }
        
        return true
    }
    
    
}

