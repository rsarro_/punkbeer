//
//  PBBaseViewController.swift
//  PunkBeer
//
//  Created by Renato Matos on 28/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import UIKit
import SnapKit
import Toast_Swift
import NVActivityIndicatorView

class PBBaseViewController: UIViewController {
    
    let activityIndicatorView: NVActivityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: UIScreen.main.bounds.size.width/2-20, y: UIScreen.main.bounds.size.height/2-20, width: 40, height: 40), type: .ballScaleRippleMultiple, color: UIColor.lightGray, padding: 0)
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showMessage(message: String) {
        self.view.makeToast(message)
    }
    
    func startLoading() {
        self.view.addSubview(self.activityIndicatorView)
        self.activityIndicatorView.startAnimating()
    }
    
    func stopLoading() {
        self.activityIndicatorView.stopAnimating()
        self.activityIndicatorView.removeFromSuperview()
    }
    
}
