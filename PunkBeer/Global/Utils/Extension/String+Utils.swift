//
//  String+Utils.swift
//  PunkBeer
//
//  Created by Renato Matos on 29/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import Foundation

extension String {
    
    var currency: String {
        let converter = NumberFormatter()
        converter.decimalSeparator = "."
        let styler = NumberFormatter()
        styler.locale = Locale(identifier: "pt-BR")
        styler.minimumFractionDigits = 2
        styler.maximumFractionDigits = 2
        styler.currencySymbol = "R$"
        styler.numberStyle = .currency
        if let result = converter.number(from: self) {
            return styler.string(from: result)!
        } else {
            converter.decimalSeparator = ","
            if let result = converter.number(from: self) {
                return styler.string(from: result)!
            }
        }
        return ""
    }
    
    var currencyValue: String {
        let converter = NumberFormatter()
        converter.decimalSeparator = "."
        let styler = NumberFormatter()
        styler.locale = Locale(identifier: "pt-BR")
        styler.minimumFractionDigits = 2
        styler.maximumFractionDigits = 2
        styler.currencySymbol = ""
        styler.numberStyle = .currency
        if let result = converter.number(from: self) {
            return styler.string(from: result)!
        } else {
            converter.decimalSeparator = ","
            if let result = converter.number(from: self) {
                return styler.string(from: result)!
            }
        }
        return ""
    }
    
    var floatConverter: Float {
        let converter = NumberFormatter()
        converter.locale = Locale(identifier: "pt-BR")
        converter.decimalSeparator = "."
        if let result = converter.number(from: self) {
            return result.floatValue
        } else {
            converter.decimalSeparator = ","
            if let result = converter.number(from: self) {
                return result.floatValue
            }
        }
        return 0
    }
    
}
