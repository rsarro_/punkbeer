//
//  ViewCodeProtocol.swift
//  PunkBeer
//
//  Created by Renato Matos on 28/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import Foundation

protocol CodableView {
    func setupViews()
    func configViews()
    func buildViews()
    func configConstraints()
}

extension CodableView {
    func setupViews() {
        self.configViews()
        self.buildViews()
        self.configConstraints()
    }
}
