//
//  EnvironmentUtil.swift
//  PunkBeer
//
//  Created by Renato Matos on 29/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import Foundation

struct EnviromentUtil {
    
    static var enviroment:[String:AnyObject]? {
        get {
            if let env = Bundle.main.infoDictionary!["EnvironmentConfig"] as? [String : AnyObject] {
                return env
            } else {
                return nil
            }
        }
    }
    
    static var baseUrl:String? {
        get {
            if let url = enviroment?["BASE_URL"] as? String{
                print(url)
                return url
            }else{
                return nil
            }
        }
    }
    
}
