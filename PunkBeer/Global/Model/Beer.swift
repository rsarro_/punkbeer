//
//  Beer.swift
//  PunkBeer
//
//  Created by Renato Matos on 29/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import Foundation
import ObjectMapper

public class Beer : Mappable {
    
    var id: Int?
    var name: String?
    var image: String?
    var tagline: String?
    var alcoholContent: Double?
    var bitternessScale: Double?
    var description: String?
    
    public required init?(map: Map) {
        //
    }
    
    public func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        image <- map["image_url"]
        tagline <- map["tagline"]
        alcoholContent <- map["abv"]
        bitternessScale <- map["ibu"]
        description <- map["description"]
    }
}
