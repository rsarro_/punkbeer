//
//  APIClient.swift
//  PunkBeer
//
//  Created by Renato Matos on 29/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

public typealias SuccessCompletion<T : Any> = (_ response : T) -> ()
public typealias FailureCompletion = (_ error : String?) -> ()


public class APIClient {
    
    public static let shared = APIClient()
    
    private var baseURL :String
    
    private init() {
        self.baseURL = ""
    }
    
    public func setupClient(baseURL :String){
        self.baseURL = baseURL
    }
    
    public func getBeers(page: Int, success: @escaping SuccessCompletion<[Beer]>, failure: @escaping FailureCompletion){
        
        Alamofire.request(self.baseURL + "beers?page=\(page)").responseArray { (response: DataResponse<[Beer]>) in
            if let list = response.result.value {
                success(list)
            } else {
                failure("Não foi possível processar a solicitação. Tente novamente mais tarde.")
            }
        }
    }
    
}
