//
//  DetailBeerViewController+Config.swift
//  PunkBeer
//
//  Created by Renato Matos on 28/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import UIKit
import PNChart

extension DetailBeerViewController {
    
    func configViews() {
        
        self.view.backgroundColor = UIColor.white
        
        self.labelTitle.text = self.model.name
        self.labelTitle.textAlignment = .center
        self.labelTitle.numberOfLines = 0
        self.labelTitle.font = UIFont(name: "Arial", size: 14)
        self.labelTitle.textColor = UIColor.black
        
        self.labelTags.text = self.model.tagline
        self.labelTags.textAlignment = .center
        self.labelTags.numberOfLines = 0
        self.labelTags.font = UIFont(name: "Arial", size: 16)
        self.labelTags.textColor = UIColor.lightGray
        
        if let image = self.model.image {
            self.imageIcon.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "ic_beer"))
            self.imageIcon.contentMode = .scaleAspectFit
        }
        
        self.labelDescription.text = self.model.description
        self.labelDescription.numberOfLines = 0
        self.labelDescription.font = UIFont(name: "Arial", size: 12)
        self.labelDescription.textColor = UIColor.lightGray
        
        self.labelTeor.text = "Teor Alcoólico"
        self.labelTeor.textAlignment = .center
        self.labelTeor.numberOfLines = 0
        self.labelTeor.font = UIFont(name: "Arial", size: 12)
        self.labelTeor.textColor = UIColor.gray
        
        self.labelAmargor.text = "Escala de Margor"
        self.labelAmargor.textAlignment = .center
        self.labelAmargor.numberOfLines = 0
        self.labelAmargor.font = UIFont(name: "Arial", size: 12)
        self.labelAmargor.textColor = UIColor.gray
        
    }
    
    func showChart() {
        
        let dimension = self.view.frame.size.width/2 - 40
        let posX = self.viewAlcoholChart.frame.size.width/2 - dimension/2
        
        self.chartAlcohol = PNCircleChart(frame: CGRect(x: posX, y: 0, width: dimension, height: dimension), total: 100.0, current: 0.0, clockwise: true, shadow: true, shadowColor: UIColor.lightGray)
        self.chartAlcohol.alpha = 0
        
        self.chartAScale = PNCircleChart(frame: CGRect(x: posX, y: 0, width: dimension, height: dimension), total: 100.0, current: 0.0, clockwise: true, shadow: true, shadowColor: UIColor.lightGray)
        self.chartAScale.alpha = 0
        
        self.chartAScale.strokeColor = UIColor(colorLiteralRed: 201/255, green: 211/255, blue: 0/255, alpha: 1)
        self.chartAScale.countingLabel.isHidden = false
        self.chartAScale.displayAnimated = true
        self.chartAScale.duration = 0.0
        self.chartAScale.stroke()
        
        self.chartAlcohol.strokeColor = UIColor(colorLiteralRed: 54/255, green: 172/255, blue: 197/255, alpha: 1)
        self.chartAlcohol.countingLabel.isHidden = false
        self.chartAlcohol.displayAnimated = true
        self.chartAlcohol.duration = 0.0
        self.chartAlcohol.stroke()
        
        self.viewAlcoholChart.addSubview(self.chartAlcohol)
        self.viewAScale.addSubview(self.chartAScale)
        
        UIView.animate(withDuration: 0.2, animations: {
            
            self.chartAScale.alpha = 1
            self.chartAlcohol.alpha = 1
            
        }) { (finish) in
            if finish {
                
                if let scale = self.model.bitternessScale {
                    self.chartAScale.current = NSNumber(value: scale)
                } else {
                    self.chartAScale.current = 0
                }
                
                self.chartAScale.stroke()
                
                self.chartAlcohol.current = NSNumber(value: self.model.alcoholContent!)
                self.chartAlcohol.stroke()
                
            }
        }
        
    }
}
