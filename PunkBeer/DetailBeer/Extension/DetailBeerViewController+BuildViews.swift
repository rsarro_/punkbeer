//
//  DetailBeerViewController+BuildViews.swift
//  PunkBeer
//
//  Created by Renato Matos on 28/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import Foundation

extension DetailBeerViewController {
    
    func buildViews() {
        
        self.view.addSubview(self.scrollView)
        self.scrollView.addSubview(self.content)
        
        self.content.addSubview(self.imageIcon)
        
        self.content.addSubview(self.labelTitle)
        self.content.addSubview(self.labelTags)
        
        self.content.addSubview(self.labelTeor)
        self.content.addSubview(self.labelAmargor)
        self.content.addSubview(self.viewAlcoholChart)
        self.content.addSubview(self.viewAScale)
        
        self.content.addSubview(self.labelDescription)
        
    }
}
