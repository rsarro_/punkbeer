//
//  DetailBeerViewController+Constraints.swift
//  PunkBeer
//
//  Created by Renato Matos on 28/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import Foundation

extension DetailBeerViewController {
    
    func configConstraints() {
        
        self.scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top)
            make.left.equalTo(self.view.snp.left)
            make.right.equalTo(self.view.snp.right)
            make.height.equalTo(self.view.snp.height)
        }
        
        self.content.snp.makeConstraints { (make) in
            make.top.equalTo(self.scrollView.snp.top)
            make.width.equalTo(UIScreen.main.bounds.size.width)
            make.bottom.equalTo(self.labelDescription.snp.bottom).offset(20)
            make.centerX.equalTo(self.scrollView.snp.centerX)
        }
        
        self.imageIcon.snp.makeConstraints { (make) in
            make.height.equalTo(100)
            make.top.equalTo(self.content.snp.top).offset(20)
            make.centerX.equalTo(self.content.snp.centerX)
        }
        
        self.labelTitle.snp.makeConstraints { (make) in
            make.top.equalTo(self.imageIcon.snp.bottom).offset(20)
            make.left.equalTo(self.content.snp.left)
            make.right.equalTo(self.content.snp.right)
        }
        
        self.labelTags.snp.makeConstraints { (make) in
            make.top.equalTo(self.labelTitle.snp.bottom)
            make.left.equalTo(self.content.snp.left)
            make.right.equalTo(self.content.snp.right)
        }
        
        self.labelTeor.snp.makeConstraints { (make) in
            make.top.equalTo(self.labelAmargor.snp.top)
            make.centerX.equalTo(self.viewAlcoholChart.snp.centerX)
        }
        self.viewAlcoholChart.snp.makeConstraints { (make) in
            make.top.equalTo(self.labelTags.snp.bottom).offset(30)
            make.left.equalTo(20)
            make.right.equalTo(self.content.snp.centerX).offset(-10)
            make.height.equalTo(self.viewAlcoholChart.snp.width)
        }
        
        self.labelAmargor.snp.makeConstraints { (make) in
            make.top.equalTo(self.viewAScale.snp.bottom).offset(10)
            make.centerX.equalTo(self.viewAScale.snp.centerX)
        }
        self.viewAScale.snp.makeConstraints { (make) in
            make.top.equalTo(self.labelTags.snp.bottom).offset(30)
            make.right.equalTo(-20)
            make.left.equalTo(self.content.snp.centerX).offset(10)
            make.height.equalTo(self.viewAScale.snp.width)
        }
        
        self.labelDescription.snp.makeConstraints { (make) in
            make.top.equalTo(self.labelAmargor.snp.bottom).offset(20)
            make.left.equalTo(self.content.snp.left).offset(20)
            make.right.equalTo(self.content.snp.right).offset(-20)
        }
        
    }
}
