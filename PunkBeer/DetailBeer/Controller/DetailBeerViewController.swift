//
//  DetailBeerViewController.swift
//  PunkBeer
//
//  Created by Renato Matos on 28/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import UIKit
import PNChart

class DetailBeerViewController: PBBaseViewController, CodableView {
    
    let scrollView: UIScrollView = UIScrollView(frame: .zero)
    let content: UIView = UIView(frame: .zero)
    
    let imageIcon: UIImageView = UIImageView(frame: .zero)
    let labelTitle: UILabel = UILabel()
    let labelTags: UILabel = UILabel()
    let labelDescription: UILabel = UILabel()
    
    let labelTeor: UILabel = UILabel()
    let viewAlcoholChart: UIView = UIView(frame: .zero)
    var chartAlcohol: PNCircleChart = PNCircleChart(frame: CGRect(x: 9, y: 0, width: 40, height: 40), total: 100.0, current: 0.0, clockwise: false, shadow: false, shadowColor: UIColor.lightGray)
    
    let labelAmargor: UILabel = UILabel()
    let viewAScale: UIView = UIView(frame: .zero)
    var chartAScale: PNCircleChart = PNCircleChart(frame: CGRect(x: 9, y: 0, width: 40, height: 40), total: 100.0, current: 0.0, clockwise: false, shadow: false, shadowColor: UIColor.lightGray)
    
    let model: Beer
    
    init(model: Beer) {
        self.model = model
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Detail"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.showChart()
        self.scrollView.contentSize = self.content.frame.size
    }
    
}
