//
//  ListBeerViewController.swift
//  PunkBeer
//
//  Created by Renato Matos on 28/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import UIKit

public typealias Final = () -> ()

class ListBeerViewController: PBBaseViewController, CodableView {
    
    let tableView: UITableView = UITableView(frame: .zero)
    var listItens: [Beer] = []
    var page: Int = 1 {
        didSet {
            self.getBeers {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "List"
        
        self.page = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.setupViews()
        self.registerCell()
    }
    
    func getBeers(finaly: @escaping Final) {
        
        self.startLoading()
        
        APIClient.shared.getBeers(page: self.page, success: { (object) in
            
            self.stopLoading()
            
            if self.page > 1 {
                self.listItens.append(contentsOf: object)
            } else {
                self.listItens = object
            }
            
            finaly()
            
        }) { (error) in
            
            self.stopLoading()
            
            if let error = error {
                self.showMessage(message: error)
            }
            finaly()
            
        }
    }
    
}
