//
//  ListBeerViewController+Constraints.swift
//  PunkBeer
//
//  Created by Renato Matos on 28/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import Foundation

extension ListBeerViewController {
    
    func configConstraints() {
        
        self.tableView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top)
            make.bottom.equalTo(self.view.snp.bottom)
            make.left.equalTo(self.view.snp.left)
            make.right.equalTo(self.view.snp.right)
        }
    }
}
