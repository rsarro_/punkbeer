//
//  ListBeerViewController+TableView.swift
//  PunkBeer
//
//  Created by Renato Matos on 28/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import UIKit

extension ListBeerViewController: UITableViewDataSource, UITableViewDelegate {
    
    func registerCell() {
        self.tableView.register(ListBeerCell.self, forCellReuseIdentifier: NSStringFromClass(ListBeerCell.self))
    }
    
    //MARK: Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listItens.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(ListBeerCell.self), for: indexPath) as! ListBeerCell
        
        cell.model = self.listItens[indexPath.row]
        
        return cell
    }
    
    //MARK: Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.listItens[indexPath.row]
        let controller = DetailBeerViewController(model: model)
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.listItens.count-2 {
            self.page = self.page + 1
        } else if indexPath.row == 2 {
            if self.page != 1 {
                self.page = 1
            }
        }
    }
    
}
