//
//  ListBeerViewController+BuildViews.swift
//  PunkBeer
//
//  Created by Renato Matos on 28/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import Foundation

extension ListBeerViewController {
    
    func buildViews() {
        self.view.addSubview(self.tableView)
    }
}
