//
//  ListBeerCell.swift
//  PunkBeer
//
//  Created by Renato Matos on 28/06/17.
//  Copyright © 2017 Renato Matos. All rights reserved.
//

import UIKit
import SDWebImage

class ListBeerCell: UITableViewCell, CodableView {
    
    var model: Beer? {
        didSet {
            self.setupViews()
        }
    }
    
    let labelTitle: UILabel = UILabel()
    let imageIcon: UIImageView = UIImageView(frame: .zero)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: CodableView Protocol
    
    func configViews() {
        
        self.selectionStyle = .none
        
        self.labelTitle.font = UIFont(name: "Arial", size: 14)
        self.labelTitle.textColor = UIColor.gray
        self.labelTitle.numberOfLines = 0
        
        self.imageIcon.contentMode = .scaleAspectFit
        
        if let name = self.model!.name {
            self.labelTitle.text = name + "\nTeor Alcoólico: " + String(format:"%.2f", self.model!.alcoholContent!).currencyValue + "%"
        }
        
        if let image = self.model!.image {
            self.imageIcon.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "ic_beer"))
        }
        
    }
    
    func buildViews() {
        
        self.addSubview(self.imageIcon)
        self.addSubview(self.labelTitle)
        
    }
    
    func configConstraints() {
        
        self.imageIcon.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.top.greaterThanOrEqualTo(20)
            make.bottom.greaterThanOrEqualTo(20)
            make.left.equalTo(20)
            make.width.equalTo(40)
            make.height.equalTo(40)
        }
        
        self.labelTitle.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.top.greaterThanOrEqualTo(20)
            make.bottom.greaterThanOrEqualTo(20)
            make.left.equalTo(self.imageIcon.snp.right).offset(10)
            make.right.equalTo(-20)
            
        }
        
    }
}
